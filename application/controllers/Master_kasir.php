<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_kasir extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		// load model terkait
		$this->load->model("kasir_model");
	}
	
	public function index()
	{
		$this->listmasterkasir();
	}
	
	public function listmasterkasir()
	{
		$data['data_master_kasir'] = $this->kasir_model->tampilDataMasterKasir();
		$this->load->view('home_kasir', $data);
	}
	public function input()
	{
		if (!empty($_REQUEST)) {
			$m_kasir = $this->kasir_model;
			$m_kasir->save();
			redirect("master_kasir/listmasterkasir", "refresh");
		}
		$this->load->view('input_master_kasir');
	}
	public function delete($no_kasir)
	{
		$m_kasir = $this->kasir_model;
		$m_kasir->delete($no_kasir);
		redirect("master_kasir/listmasterkasir", "refresh");
	}
	public function edit($no_kasir)
	{
		$data['detail_kasir'] = $this->kasir_model->detail($no_kasir);
		
		if (!empty($_REQUEST)) {
			$m_kasir = $this->kasir_model;
			$m_kasir->update($no_kasir);
			redirect("master_kasir/listmasterkasir", "refresh");
		}
		$this->load->view('edit_master_kasir', $data);
	}
}