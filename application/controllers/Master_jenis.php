<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_jenis extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		// load model terkait
		$this->load->model("jenis_model");
	}
	
	public function index()
	{
		$this->listmasterjenis();
	}
	
	public function listmasterjenis()
	{
		$data['data_master_jenis'] = $this->jenis_model->tampilDataMasterJenis();
		$this->load->view('home_jenis', $data);
	}
	
	public function input()
	{
		if (!empty($_REQUEST)) {
			$m_jenis = $this->jenis_model;
			$m_jenis->save();
			redirect("master_jenis/listmasterjenis", "refresh");
		}
		$this->load->view('input_master_jenis');
	}
	
	public function delete($kode_jenis)
	{
		$m_jenis = $this->jenis_model;
		$m_jenis->delete($kode_jenis);
		redirect("master_jenis/listmasterjenis", "refresh");
	}
	public function edit($kode_jenis)
	{
		$data['detail_jenis'] = $this->jenis_model->detail($kode_jenis);
		
		if (!empty($_REQUEST)) {
			$m_jenis = $this->jenis_model;
			$m_jenis->update($kode_jenis);
			redirect("master_jenis/listmasterjenis", "refresh");
		}
		$this->load->view('edit_master_jenis', $data);
	}
}