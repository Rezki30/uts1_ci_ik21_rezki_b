<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Trans_pembelian extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		// load model terkait
		$this->load->model("trans_pembelian_model");
		$this->load->model("kasir_model");
		$this->load->model("menu_model");
	}
	
	public function index()
	{
		$this->listtranspembelian();
	}
	
	public function listtranspembelian()
	{
		$data['data_master_kasir'] = $this->kasir_model->tampilDataMasterKasir();
		$data['data_master_menu'] = $this->menu_model->tampilDataMasterMenu();
		$data['data_pembelian'] = $this->trans_pembelian_model->tampilDataTransaksiPembelian2();
		$this->load->view('home_trans_pembelian', $data);
	}
	
	public function input()
	{
		$data['data_master_kasir'] = $this->kasir_model->tampilDataMasterKasir();
		$data['data_master_menu'] = $this->menu_model->tampilDataMasterMenu();
		$data['data_pembelian'] = $this->trans_pembelian_model->tampilDataTransaksiPembelian2();
		
		if (!empty($_REQUEST)) {
			$m_pembelian = $this->trans_pembelian_model;
			$m_pembelian->savepembelian();
			
			redirect("trans_pembelian/listtranspembelian", "refresh");
			
		}
		$this->load->view('input_trans_pembelian', $data);
	}

}