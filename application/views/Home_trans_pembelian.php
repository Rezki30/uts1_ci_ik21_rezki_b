<!DOCTYPE html>
<html>
<head>
	<title>Mr Crispy</title>
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/style/style.css">
</head>
<body bgcolor="#33FFFF">

	<header class="header">
    <h1 class="judul" align="center">Mr Crispy</h1>
    
        <div class="menu">
    <ul>
    <li><a href="#">Home</a></li>
    <li class="dropdown"><a href="#">Master</a>
    	<ul class="isi-dropdown">
    		<li><a href="<?=base_url();?>Master_kasir/listmasterkasir">Data Kasir</a></li>
            <li><a href="<?=base_url();?>Master_jenis/listmasterjenis">Data Jenis</a></li>
    		<li><a href="<?=base_url();?>Master_menu/listmastermenu">Data Menu</a></li>
    	</ul>
    </li>
    <li class="dropdown"><a href="#">Transaksi</a>
    	<ul class="isi-dropdown">
        	<li><a href="<?=base_url();?>Trans_pembelian/listtranspembelian">Pembelian</a></li>
        </ul>
    </li>
    <li><a href="#">Log ut</a></li>
    </ul>
    </div>
    </header>
    <br/>
       
        <div class="blog">
        	<div class="conteudo">
            	<div class="post-info">
        			<b>DATA PEMBELIAN</b><br>
                </div>
    
    <ul>
    <h4 align="left">
    <a href="<?=base_url();?>Trans_pembelian/input">Input Pembelian</a>
    </h4>
    </ul>
    
    <table width="100%" border="1">
      <tr align="center" bgcolor="#CCCCCC">
       <td>No</td>
        <td>Tanggal Pembelian</td>
        <td>Nama Kasir</td>
        <td>Nama Menu</td>
        <td>Qty</td>
        <td>Total Harga</td>
        </td>
      </tr>
<?php
	$no = 0;
	foreach ($data_pembelian as $data)
	{
	$no++;
?>
      <tr align="center">
         <td><?=$no;?></td>
        <td><?= $data->tgl_pembelian; ?></td>
        <td><?= $data->nama_operator; ?></td>
        <td><?= $data->nama_menu; ?></td>
        <td><?= $data->qty; ?></td>
        <td><?= $data->total_harga; ?></td>
      </tr>
<?php } ?>
    </table>
    		</div>
            <div class="footer">
  	<center><li> ©REZKI IK-021 </li></center>
       </div>
</body>
</html>