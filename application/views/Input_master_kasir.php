<!DOCTYPE html>
<html>
<head>
	<title>Mr Crispy</title>
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/style/style.css">
</head>
<body>

	<header class="header">
    <h1 class="judul" align="center">Mr Crispy</h1>
    
        <div class="menu">
    <ul>
    <li><a href="#">Home</a></li>
    <li class="dropdown"><a href="#">Master</a>
    	<ul class="isi-dropdown">
    		<li><a href="<?=base_url();?>Master_kasir/listmasterkasir">Data Kasir</a></li>
            <li><a href="<?=base_url();?>Master_jenis/listmasterjenis">Data Jenis</a></li>
    		<li><a href="<?=base_url();?>Master_menu/listmastermenu">Data Menu</a></li>
    	</ul>
    </li>
    <li class="dropdown"><a href="#">Transaksi</a>
    	<ul class="isi-dropdown">
        	<li><a href="<?=base_url();?>Trans_pembelian/listtranspembelian">Pembelian</a></li>
        </ul>
    </li>
    <li><a href="#">Log ut</a></li>
    </ul>
    </div>
    </header>
    <br/>
       
        <div class="blog">
        	<div class="conteudo">
            	<div class="post-info">
        			<b>INPUT DATA KASIR</b><br>
                </div>
            </div>
   
    <form action="<?=base_url()?>master_kasir/input" method="post">
<table width="1000px" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="#33CCFF">
  <tr>
    <td>No Kasir</td>
    <td>:</td>
    <td>
      <input type="text" name="no_kasir" id="no_kasir" maxlength="20">
    </td>
  </tr>
  <tr>
    <td>Nama Operator</td>
    <td>:</td>
    <td>
      <input type="text" name="nama_operator" id="nama_operator" maxlength="50">
    </td>
  </tr>
  <tr>
    <td>Nik Operator</td>
    <td>:</td>
    <td>
      <input type="text" name="nik_operator" id="nik_operator" maxlength="50">
    </td>
  </tr>
  <tr>
    <td>Jenis Kelamin</td>
    <td>:</td>
    <td>
      <select name="jenis_kelamin" id="jenis_kelamin">
        <option value="L">Laki-Laki</option>
        <option value="P">Perempuan</option>
      </select>
    </td>
  </tr>
  <tr>
    <td>Tanggal Lahir</td>
    <td>:</td>
    <td>
   	 <select name="tgl" id="tgl">
     <?php
     	for($tgl=1;$tgl<=31;$tgl++){
	 ?>
     	<option value="<?=$tgl;?>"><?=$tgl;?></option>
     <?php
		}
	 ?>
     </select>
      <select name="bln" id="bln">
      <?php
       $bulan_n = array('Januari','Februari','Maret','April',
	   					'Mei','Juni','Juli','Agustus','September',
						'Oktober','November','Desember');
		for($bln=0;$bln<12;$bln++){
	  ?>
      <option value="<?=$bln+1;?>">
      		<?=$bulan_n[$bln];?> </option>
      <?php
		}
	  ?>
      </select>
      <select name="thn" id="thn">
      <?php
      	for($thn = date('Y')-60;$thn <= date('Y')-15;$thn++){
	  ?>
      	<option value="<?=$thn;?>"><?=$thn;?></option>
      <?php
		}
	  ?>
      </select>

    </td>
  </tr>
  <tr>
    <td>Telepon</td>
    <td>:</td>
    <td><input type="text" name="telp" id="telp" /></td>
  </tr>
  <tr>
    <td>Alamat</td>
    <td>:</td>
    <td><textarea name="alamat" id="alamat" cols="45" rows="5"></textarea></td>
  </tr>
  
  
  <tr>
    <td></td>
    <td></td>
    <td>
    <input type="submit" name="Submit" id="Submit" value="Simpan">
    <input type="reset" name="reset" id="reset" value="Batal">
    </td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>
    <a href="<?=base_url();?>master_kasir/listmasterkasir">
    <input type="button" name="Submit" id="Submit" value="Kembali Ke Menu Sebelumnya"></a>
    </td>
  </tr>
</table>
</form>
</div>
</body>
</html>