<!DOCTYPE html>
<html>
<head>
	<title>Mr Crispy</title>
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/style/style.css">
</head>
<body>

	<header class="header">
    <h1 class="judul" align="center">Mr Crispy</h1>
    
        <div class="menu">
    <ul>
    <li><a href="#">Home</a></li>
    <li class="dropdown"><a href="#">Master</a>
    	<ul class="isi-dropdown">
    		<li><a href="<?=base_url();?>Master_kasir/listmasterkasir">Data Kasir</a></li>
            <li><a href="<?=base_url();?>Master_jenis/listmasterjenis">Data Jenis</a></li>
    		<li><a href="<?=base_url();?>Master_menu/listmastermenu">Data Menu</a></li>
    	</ul>
    </li>
    <li class="dropdown"><a href="#">Transaksi</a>
    	<ul class="isi-dropdown">
        	<li><a href="<?=base_url();?>Trans_pembelian/listtranspembelian">Pembelian</a></li>
        </ul>
    </li>
    <li><a href="#">Log ut</a></li>
    </ul>
    </div>
    </header>
    <br/>
       
        <div class="blog">
        	<div class="conteudo">
            	<div class="post-info">
        			<b>EDIT DATA KASIR</b><br>
                </div>
            </div>
<?php
	foreach ($detail_kasir as $data) {
		$no_kasir		= $data->no_kasir;
		$nama_operator	= $data->nama_operator;
		$nik_operator	= $data->nik_operator;
		$jenis_kelamin	= $data->jenis_kelamin;
		$tgl_lahir		= $data->tgl_lahir;
		$telp			= $data->telp;
		$alamat			= $data->alamat;
	}
	//Pisah bulan, tanggal, tahun
	$tahun_pisah = substr($tgl_lahir, 0, 4);
	$bulan_pisah = substr($tgl_lahir, 5, 2);
	$tanggal_pisah = substr($tgl_lahir, 8, 2);
?>
    <form action="<?=base_url()?>master_kasir/edit/<?=$no_kasir;?>" method="post">

<table width="1000px" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="#CCCCCC">
  <tr>
    <td>No Kasir</td>
    <td>:</td>
    <td>
      <input type="text" name="no_kasir" id="no_kasir" value="<?=$no_kasir;?>" maxlength="20" readonly>
    </td>
  </tr>
  <tr>
    <td>Nama Operator</td>
    <td>:</td>
    <td>
      <input type="text" name="nama_operator" id="nama_operator" value="<?=$nama_operator;?>" maxlength="50">
    </td>
  </tr>
  <tr>
    <td>Nik Operator</td>
    <td>:</td>
    <td>
      <input type="text" name="nik_operator" id="nik_operator" value="<?=$nik_operator;?>" maxlength="50">
    </td>
  </tr>
  <tr>
    <td>Jenis Kelamin</td>
    <td>:</td>
    <td>
      <?php
    	if($jenis_kelamin == 'P'){
			$slc_p = 'SELECTED';
			$slc_l = '';
		}elseif($jenis_kelamin == 'L'){
			$slc_l = 'SELECTED';
			$slc_p = '';
		}else{
			$slc_p = '';
			$slc_l = '';
		}
	?>
      <select name="jenis_kelamin" id="jenis_kelamin">
      	<option <?=$slc_p;?> value="P">Perempuan</option>
        <option <?=$slc_l;?> value="L">Laki-Laki</option>
       </select>
    </td>
  </tr>
  <tr>
    <td>Tanggal Lahir</td>
    <td>:</td>
    <td>
   	 <select name="tgl" id="tgl">
     <?php
     	for($tgl_pisah=1;$tgl_pisah<=31;$tgl_pisah++){
			$select_tgl = ($tgl_pisah == $tanggal_pisah) ? 'selected' : '';
	 ?>
     	<option value="<?=$tgl_pisah;?>" <?=$select_tgl;?>><?=$tgl_pisah;?></option>
     <?php
		}
	 ?>
     </select>
      <select name="bln" id="bln">
      <?php
       $bulan_n = array('','Januari','Februari','Maret','April',
	   					'Mei','Juni','Juli','Agustus','September',
						'Oktober','November','Desember');
		for($bln=1;$bln<=12;$bln++){
			$select_bln = ($bln == $bulan_pisah) ? 'selected' : '';
	  ?>
      <option value="<?=$bln;?>" <?=$select_bln;?>><?=$bulan_n[$bln];?> </option>
      <?php
		}
	  ?>
      </select>
      <select name="thn" id="thn">
      <?php
      	for($thn = 1990; $thn <= date('Y');$thn++){
			$select_thn = ($thn == $tahun_pisah) ? 'selected' : '';
	  ?>
      	<option value="<?=$thn;?>" <?=$select_thn;?>><?=$thn;?></option>
      <?php
		}
	  ?>
      </select>

    </td>
  </tr>
  <tr>
    <td>Telepon</td>
    <td>:</td>
    <td><input type="text" name="telp" id="telp" value="<?=$telp?>" /></td>
  </tr>
  <tr>
    <td>Alamat</td>
    <td>:</td>
    <td><textarea name="alamat" id="alamat" cols="45" rows="5"><?=$alamat;?></textarea></td>
  </tr>
 
  
  <tr>
    <td></td>
    <td></td>
    <td>
    <input type="submit" name="Submit" id="Submit" value="Simpan">
    <input type="reset" name="reset" id="reset" value="Batal">
    </td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>
    <a href="<?=base_url();?>master_kasir/listmasterkasir">
    <input type="button" name="Submit" id="Submit" value="Kembali Ke Menu Sebelumnya"></a>
    </td>
  </tr>
  </form>
</table>
</div>
</body>
</html>