<!DOCTYPE html>
<html>
<head>
	<title>Mr Crispy</title>
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/style/style.css">
</head>
<body>

	<header class="header">
    <h1 class="judul" align="center">Mr Crispy</h1>
    
        <div class="menu">
    <ul>
    <li><a href="#">Home</a></li>
    <li class="dropdown"><a href="#">Master</a>
    	<ul class="isi-dropdown">
    		<li><a href="<?=base_url();?>Master_kasir/listmasterkasir">Data Kasir</a></li>
            <li><a href="<?=base_url();?>Master_jenis/listmasterjenis">Data Jenis</a></li>
    		<li><a href="<?=base_url();?>Master_menu/listmastermenu">Data Menu</a></li>
    	</ul>
    </li>
    <li class="dropdown"><a href="#">Transaksi</a>
    	<ul class="isi-dropdown">
        	<li><a href="<?=base_url();?>Trans_pembelian/listtranspembelian">Pembelian</a></li>
        </ul>
    </li>
    <li><a href="#">Log ut</a></li>
    </ul>
    </div>
    </header>
    <br/>
       
        <div class="blog">
        	<div class="conteudo">
            	<div class="post-info">
        			<b>DATA PEMBELIAN</b><br>
                </div>
            </div>
   
    <form action="<?=base_url()?>Trans_pembelian/input" method="post">
<table width="1000px" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="#00CCFF">
  <tr>
    <td>No Kasir</td>
    <td>:</td>
    <td>
      <select name="no_kasir" id="no_kasir">
      <?php foreach($data_master_kasir as $data) {?>
      	<option value="<?= $data->no_kasir; ?>"><?= $data->no_kasir; ?></option>
      <?php } ?>
      </select>
    </td>
  </tr>
  <tr>
    <td>Nama Menu</td>
    <td>:</td>
    <td>
      <select name="nama_menu" id="nama_menu">
      <?php foreach($data_master_menu as $data) {?>
      	<option value="<?= $data->kode_menu; ?>"><?= $data->nama_menu; ?></option>
      <?php } ?>
      </select>
    </td>
  </tr>
  <tr>
    <td>Qty</td>
    <td>:</td>
    <td><input type="text" name="qty" id="qty" /></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>
    <input type="submit" name="Submit" id="Submit" value="Proses">
    <input type="reset" name="reset" id="reset" value="Reset">
    </td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>
    <a href="<?=base_url();?>Trans_pembelian/listtranspembelian">
    <input type="button" name="Submit" id="Submit" value="Kembali Ke Menu Sebelumnya"></a>
    </td>
  </tr>
</table>
</form>
</div>
</body>
</html>