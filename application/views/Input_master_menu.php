<!DOCTYPE html>
<html>
<head>
	<title>Mr Crispy</title>
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/style/style.css">
</head>
<body>

	<header class="header">
    <h1 class="judul" align="center">Mr Crispy</h1>
    
        <div class="menu">
    <ul>
    <li><a href="#">Home</a></li>
    <li class="dropdown"><a href="#">Master</a>
    	<ul class="isi-dropdown">
    		<li><a href="<?=base_url();?>Master_kasir/listmasterkasir">Data Kasir</a></li>
            <li><a href="<?=base_url();?>Master_jenis/listmasterjenis">Data Jenis</a></li>
    		<li><a href="<?=base_url();?>Master_menu/listmastermenu">Data Menu</a></li>
    	</ul>
    </li>
    <li class="dropdown"><a href="#">Transaksi</a>
    	<ul class="isi-dropdown">
        	<li><a href="<?=base_url();?>Trans_pembelian/listtranspembelian">Pembelian</a></li>
        </ul>
    </li>
    <li><a href="#">Log ut</a></li>
    </ul>
    </div>
    </header>
    <br/>
       
        <div class="blog">
        	<div class="conteudo">
            	<div class="post-info">
        			<b>INPUT DATA MENU</b><br>
                </div>
            </div>
   
    <form action="<?=base_url()?>master_menu/input" method="post">
<table width="1000px" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="#00CCFF">
  <tr>
    <td>Kode Menu</td>
    <td>:</td>
    <td>
      <input type="text" name="kode_menu" id="kode_menu" maxlength="20">
    </td>
  </tr>
  <tr>
    <td>Nama Menu</td>
    <td>:</td>
    <td>
      <input type="text" name="nama_menu" id="nama_menu" maxlength="50">
    </td>
  </tr>
  <tr>
    <td>Harga</td>
    <td>:</td>
    <td><input name="harga" id="harga" cols="45" rows="5" /></td>
  </tr>
  <tr>
    <td>Nama Jenis</td>
    <td>:</td>
    <td>
    <select name="kode_jenis" id="kode_jenis">
      <?php foreach($data_master_jenis as $data) {?>
      	<option value="<?= $data->kode_jenis; ?>"><?= $data->nama_jenis; ?></option>
      <?php } ?>
    </select></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>
    <input type="submit" name="Submit" id="Submit" value="Simpan">
    <input type="reset" name="reset" id="reset" value="Batal">
    </td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>
    <a href="<?=base_url();?>master_menu/listmastermenu">
    <input type="button" name="Submit" id="Submit" value="Kembali Ke Menu Sebelumnya"></a>
    </td>
  </tr>
</table>
</form>
</div>
</body>
</html>