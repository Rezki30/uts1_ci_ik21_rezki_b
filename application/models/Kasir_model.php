<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Kasir_model extends CI_Model
{
	//panggil nama table
	private $_table = "master_kasir";
	
	public function tampilDataMasterKasir()
	{
		// seperti : select * from <nama_table>
		return $this->db->get($this->_table)->result();
	
	}
	
	public function tampilDataMasterKasir2()
	{
		$query = $this->db->query("SELECT * FROM master_kasir WHERE flag = 1");
		return $query->result();
	
	}
	
	public function tampilDataMasterKasir3()
	{
		$this->db->select('*');
		$this->db->order_by('no_kasir', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	public function detail($no_kasir)
	{
		$this->db->select('*');
		$this->db->where('no_kasir', $no_kasir);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	public function save()
	{
		$tgl = $this->input->post('tgl');
		$bln = $this->input->post('bln');
		$thn = $this->input->post('thn');
		$tgl_gabung = $thn . "-" . $bln . "-" . $tgl;
		
		$data['no_kasir']		= $this->input->post('no_kasir');
		$data['nama_operator']	= $this->input->post('nama_operator');
		$data['nik_operator']	= $this->input->post('nik_operator');
		$data['jenis_kelamin']	= $this->input->post('jenis_kelamin');
		$data['tgl_lahir']		= $tgl_gabung;
		$data['telp']			= $this->input->post('telp');
		$data['alamat']			= $this->input->post('alamat');
		$data['flag']			= 1;
		$this->db->insert($this->_table, $data);
		
	}
	
	public function delete($no_kasir)
	{
		//delete from db
		$this->db->where('no_kasir', $no_kasir);
		$this->db->delete($this->_table);
	}
	
	public function update($no_kasir)
	{
		$tgl = $this->input->post('tgl');
		$bln = $this->input->post('bln');
		$thn = $this->input->post('thn');
		$tgl_gabung = $thn . "-" . $bln . "-" . $tgl;
		
		$data['nama_operator']	= $this->input->post('nama_operator');
		$data['nik_operator']	= $this->input->post('nik_operator');
		$data['jenis_kelamin']	= $this->input->post('jenis_kelamin');
		$data['tgl_lahir']		= $tgl_gabung;
		$data['telp']			= $this->input->post('telp');
		$data['alamat']			= $this->input->post('alamat');
		$data['flag']			= 1;
		$this->db->where('no_kasir', $no_kasir);
		$this->db->update($this->_table, $data);
	}
}
