<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Trans_pembelian_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		//load model terkait
		$this->load->model("menu_model");
    }

	//panggil nama table
	private $_table = "transaksi";
	
	public function tampilDataTransaksiPembelian()
	{
		// seperti : select * from <nama_table>
		return $this->db->get($this->_table)->result();
	
	}
	
	public function tampilDataTransaksiPembelian2()
	{
		$query = $this->db->query("SELECT * FROM transaksi as tp
		INNER JOIN master_kasir as kr on tp.no_kasir=kr.no_kasir
		INNER JOIN master_menu as mn ON tp.kode_menu= mn.kode_menu");
		$data = $query->result();
			
		return $query->result();
	
	}
	
	public function tampilDataTransaksiPembelian3()
	{
		$this->db->select('*');
		$this->db->order_by('id_transaksi', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	public function savepembelian()
	{
        $no_kasir     		= $this->input->post('no_kasir');
        $kode_menu          = $this->input->post('nama_menu');
        $qty                = $this->input->post('qty');
		$hargamenu          = $this->menu_model->cariHargaMenu($kode_menu);
		
		$data['tgl_pembelian']	= date('Y-m-d');
		$data['no_kasir']		= $no_kasir;
		$data['kode_menu']		= $kode_menu;
		$data['qty']			= $qty;
		$data['total_harga']	= $qty * $hargamenu;
		
		$this->db->insert($this->_table, $data);
	}
}
