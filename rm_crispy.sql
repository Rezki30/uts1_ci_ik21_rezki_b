-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 28, 2019 at 06:34 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rm_crispy`
--

-- --------------------------------------------------------

--
-- Table structure for table `master_jenis`
--

CREATE TABLE `master_jenis` (
  `kode_jenis` varchar(5) NOT NULL,
  `nama_jenis` varchar(30) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_jenis`
--

INSERT INTO `master_jenis` (`kode_jenis`, `nama_jenis`, `flag`) VALUES
('123', 'aksesoris', 1),
('KJ001', 'Makanan', 1),
('KJ002', 'Minuman', 1),
('KJ003', 'Barang', 1);

-- --------------------------------------------------------

--
-- Table structure for table `master_kasir`
--

CREATE TABLE `master_kasir` (
  `no_kasir` varchar(5) NOT NULL,
  `nama_operator` varchar(100) NOT NULL,
  `nik_operator` varchar(5) NOT NULL,
  `jenis_kelamin` varchar(1) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `telp` varchar(15) NOT NULL,
  `alamat` text NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_kasir`
--

INSERT INTO `master_kasir` (`no_kasir`, `nama_operator`, `nik_operator`, `jenis_kelamin`, `tgl_lahir`, `telp`, `alamat`, `flag`) VALUES
('2', 'Gilang', 'gg', 'L', '1990-01-17', '082345560131', 'gghcf', 1),
('KSR01', 'Rena Angela', '19021', 'P', '1992-06-13', '02134848334', 'Jl.Cempaka Putih Barat No.89', 1),
('KSR02', 'Hani Pertiwi', '19022', 'P', '1990-10-23', '0218675575', 'Jl.Cilincing Raya No.27', 1),
('KSR03', 'REZKI', '19023', 'P', '1975-06-15', '021867576', 'Jl.Walang Jaya', 1);

-- --------------------------------------------------------

--
-- Table structure for table `master_menu`
--

CREATE TABLE `master_menu` (
  `kode_menu` varchar(5) NOT NULL,
  `nama_menu` varchar(100) NOT NULL,
  `harga` float NOT NULL,
  `kode_jenis` varchar(5) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_menu`
--

INSERT INTO `master_menu` (`kode_menu`, `nama_menu`, `harga`, `kode_jenis`, `flag`) VALUES
('1234', 'Ayam Goreng', 1200000, 'KJ001', 1),
('KM001', 'Ayam Geprek Mozarella', 25000, 'KJ001', 1),
('KM002', 'Soda Gembira', 10000, 'KJ002', 1),
('KM003', 'Mie Mangkuk', 15000, 'KJ003', 1);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` int(11) NOT NULL,
  `tgl_pembelian` date NOT NULL,
  `no_kasir` varchar(5) NOT NULL,
  `kode_menu` varchar(5) NOT NULL,
  `qty` int(11) NOT NULL,
  `total_harga` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `tgl_pembelian`, `no_kasir`, `kode_menu`, `qty`, `total_harga`) VALUES
(1, '2019-02-19', 'KSR01', 'KM001', 2, 50000),
(2, '2019-02-19', 'KSR01', 'KM002', 1, 10000),
(3, '2019-02-28', 'KSR01', 'KM001', 2, 50000),
(4, '2019-02-28', 'KSR01', 'KM001', 4, 100000),
(5, '2019-02-28', '2', '1234', 5, 6000000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `master_jenis`
--
ALTER TABLE `master_jenis`
  ADD PRIMARY KEY (`kode_jenis`);

--
-- Indexes for table `master_kasir`
--
ALTER TABLE `master_kasir`
  ADD PRIMARY KEY (`no_kasir`);

--
-- Indexes for table `master_menu`
--
ALTER TABLE `master_menu`
  ADD PRIMARY KEY (`kode_menu`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_transaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
